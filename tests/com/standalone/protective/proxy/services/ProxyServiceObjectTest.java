package com.standalone.protective.proxy.services;

import com.standalone.protective.proxy.exceptions.AccessDeniedException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ProxyServiceObjectTest {

    private ProxyServiceObject underTest;

    @Test
    @DisplayName("It should granted access for user")
    void itShouldGrantedAccessForUser() {
        underTest = new ProxyServiceObject(new RealServiceObject(), "Kate", new Repository());

        assertDoesNotThrow(() -> underTest.process());
    }

    @Test
    @DisplayName("It should deny access for user")
    void itShouldDenyAccessForUser() {
        underTest = new ProxyServiceObject(new RealServiceObject(), "foo", new Repository());
        assertThrows(AccessDeniedException.class, () -> underTest.process());
    }

}