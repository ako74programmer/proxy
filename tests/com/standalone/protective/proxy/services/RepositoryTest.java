package com.standalone.protective.proxy.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RepositoryTest {

    @Test
    @DisplayName("It should return authorized users")
    void itShouldReturnAuthorizedUsers() {
        Repository underTest = new Repository();

        List<String> authorizeUsers = underTest.getAuthorizeUsers();

        assertAll(
                () -> assertEquals(2, authorizeUsers.size()),
                () -> assertEquals("kate", authorizeUsers.get(0)),
                () -> assertEquals("john", authorizeUsers.get(1))
        );

    }

}