package com.standalone.protective.proxy.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class RealServiceObjectTest {

    private RealServiceObject underTest;

    @Test
    @DisplayName("It should process service")
    void itShouldProcessService() {
      underTest = new RealServiceObject();

        Assertions.assertDoesNotThrow(
                () -> underTest.process()
        );
    }

}