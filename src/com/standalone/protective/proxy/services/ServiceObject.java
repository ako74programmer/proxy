package com.standalone.protective.proxy.services;

public interface ServiceObject {
    static ServiceObject createFor(String userName) {
        return new ProxyServiceObject(new RealServiceObject(), userName, new Repository());
    }

    void process();
}
