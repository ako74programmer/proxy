package com.standalone.protective.proxy.services;

import java.util.List;

public class Repository {

    public List<String> getAuthorizeUsers() {
        return List.of("kate", "john");
    }
}
