package com.standalone.protective.proxy.services;

import java.util.logging.Logger;

class RealServiceObject implements ServiceObject {
    private final Logger logger = Logger.getLogger(RealServiceObject.class.getName());

    @Override
    public void process() {
        logger.info(() -> "Process service in real object");
    }
}
