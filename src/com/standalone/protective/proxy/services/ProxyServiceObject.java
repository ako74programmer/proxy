package com.standalone.protective.proxy.services;

import com.standalone.protective.proxy.exceptions.AccessDeniedException;

import java.util.List;
import java.util.logging.Logger;

class ProxyServiceObject implements ServiceObject {
    private final Logger logger = Logger.getLogger(ProxyServiceObject.class.getName());

    private final RealServiceObject realServiceObject;
    private final Repository repository;
    private final String userName;

    public ProxyServiceObject(RealServiceObject realServiceObject, String userName, Repository repository) {
        this.realServiceObject = realServiceObject;
        this.repository = repository;
        this.userName = userName;
    }

    @Override
    public void process() {
        List<String> authorizedUsers = repository.getAuthorizeUsers();
        if (!authorizedUsers.contains(userName.toLowerCase())) {
            throw new AccessDeniedException("Access Denied: User " + userName + " is not allowed!");
        }
        logger.info("Access Granted");
        realServiceObject.process();
    }


}

