package com.standalone.protective.proxy;

import com.standalone.protective.proxy.services.ServiceObject;

public class Demo {
    public static void main(String[] args) {
        ServiceObject serviceObject = ServiceObject.createFor("Kate");
        serviceObject.process();
    }
}
